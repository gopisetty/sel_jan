package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] readData(String dataSheetname) throws IOException {
		// TODO Auto-generated method stub

		XSSFWorkbook wbook = new XSSFWorkbook("./Data/readdata.xlsx");//Enter workbook
		XSSFSheet sheet = wbook.getSheet("Sheet1");//Enter the sheet
		
		
		int rowCount = sheet.getLastRowNum();//Getting row count
		
		//getLastRowNum() :- this method will ignore the header
		
		System.out.println(rowCount);
		
		int colCount = sheet.getRow(0).getLastCellNum();//getting cell count for header row
		System.out.println("column count" +colCount);
		Object[][] data = new Object[rowCount][colCount];
		for(int i = 1 ; i<=rowCount;i++)
		{
			XSSFRow row = sheet.getRow(i); //Entering row 
			
			for(int j =0 ; j<colCount;j++) //Enter cell
			{
				XSSFCell cell =row.getCell(j); //Getting value from cell
			
				data[i-1][j] = cell.getStringCellValue();
				//String text = cell.getStringCellValue();
				//System.out.println(text);
			}
			
			
			
		}
		return data;
		
		
	}

}

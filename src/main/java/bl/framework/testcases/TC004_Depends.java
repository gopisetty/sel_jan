package bl.framework.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC004_Depends {
	
	@Test(groups={"sanity"})
	public void openBrowser()
	{
		System.out.println("I am openBrowser");
	}
	@BeforeMethod(groups= {"sanity"})
	public void goToURL()
	{
		System.out.println("I am goToURL");
	}
    @AfterMethod(groups= {"sanity"})
	public void clickGmail()
	{
		System.out.println("I am clickGmail");
	}
	
}
